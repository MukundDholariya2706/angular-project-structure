import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { CoreCommonModule } from "@core/common.module";
import { CoreModule } from "@core/core.module";
import { NgSelectModule } from "@ng-select/ng-select";
import { coreConfig } from "app/app-config";
import { ToastrModule } from "ngx-toastr";
import { LoginComponent } from "./login.component";

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        CoreCommonModule,
        CoreModule.forRoot(coreConfig),
        ToastrModule.forRoot(),
        NgSelectModule
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
