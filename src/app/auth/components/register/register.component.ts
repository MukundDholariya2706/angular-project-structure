import { RegisterUserModel } from './../../models/register-user.model';
import { Router } from '@angular/router';
import { AuthenticationService } from 'app/auth/service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { CoreConfigService } from '@core/services/config.service';
import { Subject } from 'rxjs';
import { first, take, takeUntil } from 'rxjs/operators';
import { MustMatch, NotMatch } from 'app/core/helper/password.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
  // Public
  public coreConfig: any;
  public passwordTextType: boolean;
  public showConfirmPassword: boolean;
  public registerForm: FormGroup;
  public submitted = false;
  public error: string = '';
  public loading = false;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(private _coreConfigService: CoreConfigService, private _formBuilder: FormBuilder, private _authenticationService: AuthenticationService, private router: Router) { 
    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  /**
   * Toggle password
   */
  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  toggleConfirmPassword() {
    this.showConfirmPassword = !this.showConfirmPassword;
  }

  /**
   * On Submit
   */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this._authenticationService.registerUser(this.registerForm.value as RegisterUserModel).pipe(first())
    .subscribe(
      (data: any) => {
        if(!!data.status){
          this.router.navigate(['auth/login']);
        }
      },
      (error) => {
        this.error = error;
        this.loading = false;
      }
    )
  }

  ngOnInit(): void {
    this.registerForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_password: ['', [Validators.required]],
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]]
    },
    {
      validator: [MustMatch('password', 'confirm_password')]
    }
    );

    // Subscribe to config changes
    this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
      this.coreConfig = config;
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
