import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreCommonModule } from './../../@core/common.module';
import { AuthRoutingModule } from './auth-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { RegisterComponent } from './components/register/register.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    NgbModule,
    CoreCommonModule,
    NgSelectModule
  ],
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    RegisterComponent,
  ]
})
export class AuthModule { }
