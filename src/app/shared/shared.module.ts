import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ImgCropperComponent } from "./components/img-cropper/img-cropper.component";
import { ImageCropperModule } from "ngx-image-cropper";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [ImgCropperComponent],
  imports: [CommonModule, ImageCropperModule, GooglePlaceModule, NgxMaskModule.forRoot()],
  exports: [ImgCropperComponent, GooglePlaceModule, NgxMaskModule ],
})
export class SharedModule {}
