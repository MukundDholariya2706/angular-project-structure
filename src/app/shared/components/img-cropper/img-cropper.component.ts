import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Component, Input, OnInit } from "@angular/core";
import { base64ToFile, ImageCroppedEvent } from "ngx-image-cropper";

@Component({
  selector: "app-img-cropper",
  templateUrl: "./img-cropper.component.html",
  styleUrls: ["./img-cropper.component.scss"],
})
export class ImgCropperComponent implements OnInit {
  @Input() public coverImageWeb!: string;
  @Input() public aspectHeight!: number;
  @Input() public aspectWidth!: number;
  @Input() public roundCropper!: boolean;
   
  public imageChangedEvent: any = "";
  public croppedImage: any = "";
  public showError = false;
  public isFileChanged = false;
  notToCrop = [
    'assets/images/profile/user-uploads/timeline.jpg'
  ]

  constructor(public model: NgbActiveModal) {}

  ngOnInit(): void {  }

  imageCropped(event: ImageCroppedEvent): void {
    this.croppedImage = event.base64;
  }

  loadImageFailed() {
    // this.toastr.showError("File not supported","");
    this.showError = true;
  }

  onFileChanged(eventOnChange: any): void {
    if (eventOnChange.target.files[0]) {
      if (
        eventOnChange.target.files[0].type === "image/png" ||
        eventOnChange.target.files[0].type === "image/jpeg" ||
        eventOnChange.target.files[0].type === "image/jpg"
      ) {
        this.isFileChanged = true;
        this.showError = false;
        this.imageChangedEvent = eventOnChange.target.files[0];
      } else {
        this.isFileChanged = false;
        this.imageChangedEvent = null;
        this.croppedImage = null;
        this.loadImageFailed();
      }
    }
  }

  onCoverImageUpload(): void {
    if (this.croppedImage) {
      if (this.isFileChanged || (!this.notToCrop.includes(this.coverImageWeb)) ) {
        const croppedImage = base64ToFile(this.croppedImage);
        this.model.close(croppedImage);
      } else {
        this.model.close();
      }
    }
  }
}
