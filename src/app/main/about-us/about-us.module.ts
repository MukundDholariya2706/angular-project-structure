import { CoreCommonModule } from '@core/common.module';
import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './about-us.component';
import { EditorModule } from '@tinymce/tinymce-angular';

const route: Route[] = [
  {
    path: '',
    component: AboutUsComponent
  }
];

@NgModule({
  declarations: [
    AboutUsComponent
  ],
  imports: [
    EditorModule,
    CommonModule,
    RouterModule.forChild(route),
    CoreCommonModule
  ]
})
export class AboutUsModule { }
