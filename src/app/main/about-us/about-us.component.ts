import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

declare let tinymce: any;
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  x: any;
  dataModel: any;
  constructor(private domSanitizer: DomSanitizer) { }

  ngOnInit(): void {
  }

  getInit(): any {
    return {
      menubar: false,
      branding: false,
      plugins: 'image emoticons',
      autosave_interval: '1s',
      toolbar:
        'table bold underline italic | blocks fontfamily fontsize | forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | image ',
      file_picker_callback: function (cb: (arg0: any) => void, value: any, meta: any) {
        const input = document.createElement('input') as HTMLInputElement | any;
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function () {
          const file = input.files[0];
          const reader = new FileReader();
          reader.onload = function () {
            const id = 'blobid' + new Date().getTime();
            const blobCache = tinymce.activeEditor.editorUpload.blobCache;
            const base64 = (<string>reader.result).split(',')[1];
            const blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);
            cb(blobInfo.blobUri());
          };
          reader.readAsDataURL(file);
        };

        input.click();
      },
    };
  }

  onButtonClick() {
    const b = tinymce.activeEditor.getBody().innerHTML;
    this.x = this.domSanitizer.bypassSecurityTrustHtml(b);
  }

}
