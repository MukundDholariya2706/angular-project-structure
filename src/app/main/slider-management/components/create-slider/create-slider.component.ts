import { SliderManagementService } from "app/main/services/slider-management.service";
import { Validators } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";
import { ImgCropperComponent } from "app/shared/components/img-cropper/img-cropper.component";
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: "app-create-slider",
  templateUrl: "./create-slider.component.html",
  styleUrls: ["./create-slider.component.scss"],
})
export class CreateSliderComponent implements OnInit {
  @Input() public sliderID: any;
  @BlockUI() blockUI: NgBlockUI;

  public createSliderForm: FormGroup;
  submitted: boolean = false;
  imageSrc: string;
  uploadFileData: any;
  fileType: string[] = ["image/png", "image/jpeg"];

  get f() {
    return this.createSliderForm.controls;
  }

  constructor(
    private _formBuilder: FormBuilder,
    private model: NgbActiveModal,
    private _toasterService: ToastrService,
    private _sliderManagementService: SliderManagementService,
    private modalService: NgbModal,
  ) {}

  ngOnInit(): void {
    this.initForm();
    if (!!this.sliderID) {
      this.getSingleSlider();
    }
  }

  initForm(): void {
    this.createSliderForm = this._formBuilder.group({
      title: ["", [Validators.required]],
      status: [false, [Validators.required]],
      slider_image: ["", []],
    });
  }

  getSingleSlider() {
    this._sliderManagementService
      .getSingleSlider(this.sliderID)
      .subscribe((res: any) => {
        if (res.status) {
          this.patchValue(res.data);
        }
      });
  }

  patchValue(data: any = {}) {
    this.imageSrc = data.slider_image;
    this.createSliderForm.patchValue({
      title: data.title,
      status: data.status == 2 ? false : true,
    });
  }

  modelDismiss(): void {
    this.model.dismiss();
  }

  onImagChangeFromFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];
      // this.uploadFileData = file;
      if (this.fileType.indexOf(file.type) >= 0) {
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        this.blockUI.start('Loading...');
        reader.onload = (e: any) => {
          this.openImageCropper(e.target.result);
        };
      } else {
        this.imageSrc = "";
        this.uploadFileData = undefined;
        this.createSliderForm.controls.slider_image.reset();
        const message = file.type + " file type not support";
        this._toasterService.error("", message, {
          toastClass: "toast ngx-toastr",
          closeButton: true,
        });
      }
    }
  }

  openImageCropper(imageUrl: string) {
    this.blockUI.stop();
    const modelRef = this.modalService.open(ImgCropperComponent, {
      centered: true,
    });

    modelRef.componentInstance.coverImageWeb = imageUrl;
    modelRef.componentInstance.aspectHeight = 6;
    modelRef.componentInstance.aspectWidth = 16;
    modelRef.componentInstance.roundCropper = false;


    modelRef.result.then(
      (file: File) => {
        this.uploadFileData = file;
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = (e: any) => {
          this.imageSrc = e.target.result;
          this.createSliderForm.patchValue({
            slider_image: file,
          });
        };
      },
      () => {}
    )
  }


  submitData() {
    this.submitted = true;
    if (!!this.sliderID) {
      this.f.slider_image.removeValidators(Validators.required);
      this.f.slider_image.updateValueAndValidity();
    } else {
      this.f.slider_image.addValidators(Validators.required);
      this.f.slider_image.updateValueAndValidity();
    }

    if (this.createSliderForm.invalid) return;

    const formData = new FormData();
    for (let key in this.createSliderForm.value) {
      if (key == "status") {
        const status = this.createSliderForm.value[key] === false ? "2" : "1";
        formData.append(key, status);
      } else {
        if (this.createSliderForm.value[key])
          formData.append(key, this.createSliderForm.value[key]);
      }
    }

    let API: any;
    if (!!this.sliderID) {
      API = this._sliderManagementService.editSlider(formData, this.sliderID);
    } else {
      API = this._sliderManagementService.addSlider(formData);
    }

    API.subscribe((data: any) => {
      if (data.status) {
        this.model.close();
      }
    });
  }
}
