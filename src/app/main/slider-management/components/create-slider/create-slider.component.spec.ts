import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppModule } from 'app/app.module';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSliderComponent } from './create-slider.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

describe('CreateSliderComponent', () => {
  let component: CreateSliderComponent;
  let fixture: ComponentFixture<CreateSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSliderComponent ],
      imports: [AppModule, HttpClientTestingModule],
      providers: [NgbActiveModal]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
