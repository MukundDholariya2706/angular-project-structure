import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'app/app.module';

import { SliderManagementComponent } from './slider-management.component';

describe('SliderManagementComponent', () => {
  let component: SliderManagementComponent;
  let fixture: ComponentFixture<SliderManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SliderManagementComponent ],
      imports: [AppModule, HttpClientTestingModule],
    })
    .compileComponents();

    fixture = TestBed.createComponent(SliderManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
