import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { SliderManagementService } from "app/main/services/slider-management.service";
import { CreateSliderComponent } from "./create-slider/create-slider.component";
import Swal from "sweetalert2";

@Component({
  selector: "app-slider-management",
  templateUrl: "./slider-management.component.html",
  styleUrls: ["./slider-management.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class SliderManagementComponent implements OnInit {
  public sliderList: any[] = [];
  value: any;

  constructor(
    private _sliderManagementService: SliderManagementService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.getSliderList();
  }

  getSliderList() {
    this._sliderManagementService.getAllSlider().subscribe((res: any) => {
      if (res.status) {
        this.sliderList = res.data;
      }
    });
  }

  AddSlider(id: any = null) {
    const modelRef = this.modalService.open(CreateSliderComponent, {
      centered: true,
    });


    if(!!id){
      modelRef.componentInstance.sliderID = id;
    }

    modelRef.result.then(
      (data: any) => {
        this.getSliderList();
      },
      () => {}
    );
  }

  deleteSlider(sliderid: string) {
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      customClass: {
        confirmButton: "btn btn-primary",
        cancelButton: "btn btn-danger ml-1",
      },
    }).then((result: any) => {
      if (result.isConfirmed) {
        this._sliderManagementService
          .deleteSlider(sliderid)
          .subscribe((res: any) => {
            if (res.status) {
              this.getSliderList();
            }
          });
      }
    });
  }

  statusChange(event: Event, id: any) {
    let active = event.target["checked"] ? '1' : '2';
    const formData = new FormData();
    formData.append('status', active);
    this._sliderManagementService.editSlider(formData, id).subscribe((res: any) => {
        if(res.status){

        } else {
          this.getSliderList();
        }
      });
  }
}
