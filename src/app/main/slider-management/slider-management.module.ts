import { SliderManagementComponent } from './components/slider-management.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CoreCommonModule } from '@core/common.module';
import { CreateSliderComponent } from './components/create-slider/create-slider.component';


const routes: Routes = [
  { path: '', 
    component: SliderManagementComponent
  }
]


@NgModule({
  declarations: [SliderManagementComponent, CreateSliderComponent],
  imports: [
    CommonModule,
    CoreCommonModule,
    RouterModule.forChild(routes),
    NgxDatatableModule
  ]
})
export class SliderManagementModule { }
