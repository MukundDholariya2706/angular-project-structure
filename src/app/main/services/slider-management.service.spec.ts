import { delay } from "rxjs/operators";
import { SliderManagementComponent } from "./../slider-management/components/slider-management.component";
import { BaseApiService } from "./../../core/api/base-api.service";
import { SliderManagementService } from "./slider-management.service";
import { TestBed, inject, fakeAsync, tick } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController,
} from "@angular/common/http/testing";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Constants } from "app/API-URL/contants";
import { ErrorInterceptor, JwtInterceptor } from "app/auth/helpers";
import { ToastrModule } from "ngx-toastr";
import { of } from "rxjs";

describe("SliderManagementService", () => {
  let service: SliderManagementService;
  let httpTestingController: HttpTestingController;
  let baseUrl = "http://137.184.19.129:4001/v1/slider/get-slider";
  let slider: {
    _id: string;
    slider_image: string;
    title: string;
    status: number;
    created_at: number;
    updated_at: number;
    __v: number;
  };

  let token: any;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ToastrModule.forRoot()],
      providers: [HttpClientModule, BaseApiService, SliderManagementService],
    });
    service = TestBed.inject(SliderManagementService);
    httpTestingController = TestBed.inject(HttpTestingController);
    slider = {
      _id: "63eb7dcb7a865a8d389828a9",
      slider_image:
        "http://137.184.19.129:4001/uploadFile/slider_image/slider_image-1676451631052.png",
      title: "Slide",
      status: 1,
      created_at: 1676377547,
      updated_at: 1676377547,
      __v: 0,
    };

    token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2M2JkMzkwNTAzODhhMmU5Y2YxYTEzYzEiLCJmaXJzdF9uYW1lIjoiTWF4IiwibGFzdF9uYW1lIjoiQ2hhcG1hbiIsImVtYWlsIjoibXVrdW5kMTIzQHlvcG1haWwuY29tIiwidXNlcl90eXBlIjoxLCJpYXQiOjE2NzY1MjQwNDQsImV4cCI6MTY3NzczMzY0NH0.UWYE8DAU9gqfdINDctdyO1ltf-rfVcoIogoZKfAko4s";
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it("service should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should return data", () => {
    localStorage.setItem("auth_token", token);

    service.getAllSlider().subscribe((t: any) => {
      expect(t).toEqual(slider);
    });

    const req = httpTestingController.expectOne({
      url: baseUrl,
    });
    req.flush(slider);
  });
});
