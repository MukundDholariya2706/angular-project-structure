import { UsersComponent } from "./components/users/users.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CoreCommonModule } from "@core/common.module";
import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { Route, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { CoreSidebarModule } from "@core/components";

const route: Route[] = [
  {
    path: "",
    redirectTo: "all",
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: "all",
        component: UsersComponent,
      },
      {
        path: "admin",
        loadChildren: () => import('./components/admin/admin.module').then(m => m.AdminModule)      },
    ]
  }
];

@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    CoreCommonModule,
    RouterModule.forChild(route),
    ContentHeaderModule,
    NgxDatatableModule,
    NgbModule,
    CoreSidebarModule
  ],
  exports: [RouterModule],
})
export class UsersModule {}
