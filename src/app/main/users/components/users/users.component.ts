import { FilterParamsModel } from './../../models/admin.model';
import { ContentHeader } from "app/layout/components/content-header/content-header.component";
import {
  Component,
  HostListener,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";
import {
  ColumnMode,
  DatatableComponent,
  SelectionType,
} from "@swimlane/ngx-datatable";
import { debounceTime, map } from "rxjs/operators";
import { Subject } from "rxjs";
import { PER_PAGE_LIMIT } from "app/API-URL/contants";
import { UsersService } from "../../Service/users.service";
import { ResponseModel } from 'app/core/models/response.model';

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class UsersComponent implements OnInit {
  public tableOffSet: number = 0;
  public contentHeader: ContentHeader;
  public basicSelectedOption: number[] = PER_PAGE_LIMIT;
  public usersList: any[] = [];
  public ColumnMode = ColumnMode;
  public SelectionType = SelectionType;
  public selected = [];
  public pageInfo: {limit: number, page: number, total: number} = { limit: 10, page: 1, total: 0 };
  public size: string = "";
  public paginationConfig = {
    size: "",
    maxSize: 5,
  };
  public filterParams: FilterParamsModel = {
    search: undefined,
    sortBy: undefined,
    page: undefined,
    limit: 5,
    status: undefined,
    user_type: undefined,
  };

  searchTerm$ = new Subject<any>();

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private _usersService: UsersService) {
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Users",
      actionButton: true,
    };

    // server-side search
    this.searchTerm$.pipe(
      map((event) => event.target.value),
      debounceTime(500))
      .subscribe(() => {
        this.getUserList(this.filterParams);
      });

    this.getUserList(this.filterParams);
  }

  getUserList(params: any = {}) {
    this._usersService.getUsersList(params).subscribe((res: ResponseModel) => {
      if (res.status) {      
        this.usersList = res?.data == null ? [] : res?.data?.data;
        this.pageInfo.page = res?.data.page;
        this.pageInfo.limit = res?.data.limit;
        this.pageInfo["total"] = res?.data.total;
        this.filterParams.limit = res?.data.limit;
      }
    }, 
    (error => {
      this.usersList = [];
    }));
  }

  filterUpdate(event: KeyboardEvent ): void {
    const val = event.target['value'].toLowerCase();
    this.filterParams.search = val == "" ? undefined : val;
    this.filterParams.page = 1;
    this.tableOffSet = 0;
  }

  onPage(pagenumber: any): void {
    this.tableOffSet = pagenumber.offset;
    this.filterParams.page = pagenumber.offset + 1;
    this.getUserList(this.filterParams);
  }

  sortCallback(event : any){

  }

  statusChange(status: Event): void {
    this.filterParams.status = status.target['value'] == '' ? undefined : status.target['value'];
    this.filterParams.page = 1;
    this.getUserList(this.filterParams);
    this.tableOffSet = 0;
  }

  roleChange(userType: Event): void {
    this.filterParams.user_type = userType.target['value'] == '' ? undefined : userType.target['value'];
    this.filterParams.page = 1;
    this.getUserList(this.filterParams);
    this.tableOffSet = 0;
  }

  limitChange(event: Event): void {
    const limit = event.target['value'];
    this.filterParams.page = 1;
    this.filterParams.limit = limit;
    this.getUserList(this.filterParams);
    this.tableOffSet = 0;
  }
}
