import { FilterParamsModel, AdminModel } from './../../models/admin.model';
import { debounceTime, map } from "rxjs/operators";
import { PER_PAGE_LIMIT } from "app/API-URL/contants";
import { UsersService } from "./../../Service/users.service";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import Swal from "sweetalert2";
import { Subject } from "rxjs";
import { CoreSidebarService } from "@core/components/core-sidebar/core-sidebar.service";
import { ResponseModel } from 'app/core/models/response.model';

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class AdminComponent implements OnInit {
  public tableOffSet: number = 0;
  public adminUserList: AdminModel[] = [];
  public basicSelectedOption: number[] = PER_PAGE_LIMIT;
  public pageInfo: {limit: number, page: number, total: number} = { limit: 10, page: 1, total: 0 };
  public paginationConfig: {size: string ; maxSize: number} = {
    size: "",
    maxSize: 0,
  };
  public filterParams: FilterParamsModel = {
    search: undefined,
    sort: undefined,
    page: undefined,
    length: 5,
    status: undefined,
    user_type: undefined,
    direction: undefined
  };

  searchTerm$ = new Subject<any>();

  constructor(
    private _usersService: UsersService,
    private _coreSidebarService: CoreSidebarService
  ) {}

  ngOnInit(): void {
    // server-side search
    this.searchTerm$
      .pipe(
        map((event) => event.target.value),
        debounceTime(500)
      )
      .subscribe(() => {
        this.getAdminList(this.filterParams);
      });

    this._usersService.onCurrentCreateUserChange.subscribe((res: any) => {
      if (res.sucess) this.getAdminList(this.filterParams);
    });

    this.getAdminList(this.filterParams);
  }

  getAdminList(params: FilterParamsModel): void {
    this._usersService.getAdminList(params).subscribe((res: ResponseModel) => {
      if (res.status == 1) {
        this.adminUserList = res.data.adminList;
        this.pageInfo["total"] = res?.data.adminUsersCount;
      }
    });
  }

  onPage(pagenumber: any): void {
    this.tableOffSet = pagenumber.offset;
    this.filterParams.page = pagenumber.offset + 1;
    this.getAdminList(this.filterParams);
  }

  filterUpdate(event: KeyboardEvent ): void {
    const val = event.target['value'].toLowerCase();
    this.filterParams.search = val == "" ? undefined : val;
    this.filterParams.page = 1;
    this.tableOffSet = 0;
  }

  limitChange(event: Event ): void {
    const length = event.target['value'];
    this.filterParams.page = 1;
    this.filterParams.length = length;
    this.getAdminList(this.filterParams);
    this.tableOffSet = 0;
  }

  // custom pagination not use - START
  pageChange(pagenumber: any): void {
    this.filterParams.page = pagenumber;
    this.getAdminList(this.filterParams);
  }

  // custom pagination not use - END

  deleteAdmin(id: string): void {
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      customClass: {
        confirmButton: "btn btn-primary",
        cancelButton: "btn btn-danger ml-1",
      },
    }).then((result: any) => {
      if (result.isConfirmed) {
        this._usersService.deleteAdmin(id).subscribe(
          (res: any) => {
            if (res.status == 1) {
              console.log('this. :>> ', this.filterParams, this.tableOffSet);
              // this.filterParams.page = 1;
              // this.tableOffSet = 0;
              this.getAdminList(this.filterParams);
            }
          },
          (error: any) => {}
        );
      }
    });
  }

  /**
   * Sidebar - START
   */

  /**
   * Toggle Event Sidebar
   */
  toggleEventSidebar(): void {
      this._coreSidebarService.getSidebarRegistry('user-create-sidebar').toggleOpen();
    }

  AddEvent(): void {
    this.toggleEventSidebar();
    this._usersService.observerEvent();
  }

  editUser(id: string, data: any): void{
    this.toggleEventSidebar();
    this._usersService.onCurrentCreateUserChange.next({update: true, adminId: id, adminData: data });
  }

  sortCallback(event: any): void{
    this.filterParams.sort = event.sorts[0].prop == 'name' ? 'first_name' : event.sorts[0].prop;
    this.filterParams.direction = event.sorts[0].dir;
    this.filterParams.page = 1;
    this.getAdminList(this.filterParams);
    this.tableOffSet = 0;
  }
}
