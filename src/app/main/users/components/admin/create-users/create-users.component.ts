import { ROLE } from '../../../../../API-URL/contants';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { CoreSidebarService } from "@core/components/core-sidebar/core-sidebar.service";
import { UsersService } from "app/main/users/Service/users.service";
import { ResponseModel } from 'app/core/models/response.model';
import { MustMatch } from 'app/core/helper/password.validator';

@Component({
  selector: "app-create-users",
  templateUrl: "./create-users.component.html",
  styleUrls: ["./create-users.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class CreateUsersComponent implements OnInit {
  public event: any = {};
  public adminId: string;
  public isDataEmpty: boolean = true;
  public roles = ROLE;
  userForm!: FormGroup;
  submitted: boolean = false;  
  public passwordTextType = false;
  public confirmPasswordTextType = false;

  get f() {
    return this.userForm.controls;
  }

  constructor(
    private _formBuilder: FormBuilder,
    private _userService: UsersService,
    private _coreSidebarService: CoreSidebarService
  ) {}

  ngOnInit(): void {
    this._userService.onCurrentCreateUserChange.subscribe((res: any) => {
      if (Object.keys(res).length > 0) {
        this.event = res?.adminData;
        this.adminId = res?.adminId;
        this.isDataEmpty = false;
        this.initForm();
      } else {
        this.resetData();
        this.initForm();
        this.isDataEmpty = true;
      }
    });
  }

  toggleEventSidebar() {
    this.resetData();
    this._coreSidebarService.getSidebarRegistry("user-create-sidebar").toggleOpen();
  }

  initForm() {
    this.userForm = this._formBuilder.group({
      first_name: [this.event?.first_name, [Validators.required]],
      last_name: [this.event?.last_name, [Validators.required]],
      email: [this.event?.email, [Validators.required, Validators.email]],
      company: [this.event?.company, [Validators.required]],
      address: [this.event?.address, [Validators.required]],
      user_type: [this.event?.user_type, [Validators.required]]
    },
    {
      validator: [MustMatch('password', 'confirm_password')]
    });

    this.updateForm();
  }

  updateForm(){
    if(this.isDataEmpty){
      this.userForm.addControl('password', this._formBuilder.control('',[Validators.required]));
      this.userForm.addControl('confirm_password', this._formBuilder.control('',[Validators.required]));
    } else {
      this.userForm.removeControl('password');
      this.userForm.removeControl('confirm_password');
    }
  }

  resetData(): void{
    this.submitted = false;
    this.isDataEmpty = true;
    this.event = {}
  }

  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  toggleConfirmPasswordTextType() {
    this.confirmPasswordTextType = !this.confirmPasswordTextType;
  }

  submit() {
    this.submitted = true;
    if (this.userForm.invalid) return;

    let API: any;
    if (this.isDataEmpty) {
      API = this._userService.createNewUser(this.userForm.value);
    } else {
      API = this._userService.updateUser(this.userForm.value, this.adminId);
    }

    API.subscribe(
      (res: ResponseModel) => {
        if (res.status) {
          this.toggleEventSidebar();
          this._userService.onCurrentCreateUserChange.next({ sucess: true });
        }
      },
      (error: any) => {}
    );
  }
}
