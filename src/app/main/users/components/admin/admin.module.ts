import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { Route } from '@angular/router';
import { CreateUsersComponent } from './create-users/create-users.component';
import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreCommonModule } from '@core/common.module';
import { CoreSidebarModule } from '@core/components';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';

const route: Route[] = [
  {
    path: '',
    component: AdminComponent
  }
]

@NgModule({
  declarations: [AdminComponent, CreateUsersComponent],
  imports: [
    CommonModule,
    CoreCommonModule,
    ContentHeaderModule,
    CoreSidebarModule,
    RouterModule.forChild(route),
    NgxDatatableModule,
    NgSelectModule,
    NgbModule
  ],
  exports: [RouterModule]
})
export class AdminModule { }