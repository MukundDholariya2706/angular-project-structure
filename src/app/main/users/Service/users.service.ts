import { Observable, BehaviorSubject } from 'rxjs';
import { BaseApiService } from './../../../core/api/base-api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public currentCreateUser;
  public onCurrentCreateUserChange: BehaviorSubject<any>;

  constructor(private _baseApiService: BaseApiService) {
    this.onCurrentCreateUserChange = new BehaviorSubject({});
  }

  getUsersList(params: any = {}): Observable<any>{
    const filterParams = {}

    for(let param in params){
      if(params[param] !== undefined){
        filterParams[param] = params[param]
      }
    }
    return this._baseApiService.makeRequest('GET','users/users-list',filterParams);
  }

  getAdminList(params: any = {}): Observable<any>{
    const filterParams = {}
    for(let param in params){
      if(params[param] !== undefined){
        filterParams[param] = params[param]
      }
    }
    return this._baseApiService.makeRequest('GET','admin/admin-list', filterParams);
  }

  deleteAdmin(adminId : any): Observable<any>{
    return this._baseApiService.makeRequest('PUT','admin/delete-admin/' + adminId);
  }

  createNewUser(postData: any){
    return this._baseApiService.makeRequest('POST','admin/create-admin', postData);
  }

  updateUser(postData: any, adminId: any){
    return this._baseApiService.makeRequest('PUT','admin/edit-admin/'+ adminId, postData);
  }

  observerEvent(){
    this.currentCreateUser = {};
    this.onCurrentCreateUserChange.next(this.currentCreateUser);
  }
}
