import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Route, RouterModule } from '@angular/router';
import { CoreCommonModule } from "@core/common.module";
import { ContactUsComponent } from "./contact-us.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CreateContactUsComponent } from './create-contact-us/create-contact-us.component';
import { ViewContactUsComponent } from './view-contact-us/view-contact-us.component';

const route: Route[] = [
  {
    path: '',
    component: ContactUsComponent,
    data: { animation: 'fadeIn' }
  }
]

@NgModule({
  declarations: [ContactUsComponent, CreateContactUsComponent, ViewContactUsComponent],
  imports: [
    CommonModule,
    CoreCommonModule,
    RouterModule.forChild(route),
    NgxDatatableModule,
    NgbModule
  ],
  providers: []
})
export class ContactUsModule {}
