import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'app/app.module';

import { ViewContactUsComponent } from './view-contact-us.component';

describe('ViewContactUsComponent', () => {
  let component: ViewContactUsComponent;
  let fixture: ComponentFixture<ViewContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewContactUsComponent ],
      imports: [AppModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
