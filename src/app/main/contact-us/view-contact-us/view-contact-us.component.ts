import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-contact-us',
  templateUrl: './view-contact-us.component.html',
  styleUrls: ['./view-contact-us.component.scss']
})
export class ViewContactUsComponent implements OnInit {

  @Input() private data: any;
  details: any;
  constructor() { }

  ngOnInit(): void {
  this.details = this.data;
  console.log(this.details);
      
  }

}
