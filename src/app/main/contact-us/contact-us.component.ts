import { DomSanitizer } from "@angular/platform-browser";
import { PER_PAGE_LIMIT } from "./../../API-URL/contants";
import { debounceTime, map } from "rxjs/operators";
import { HostListener, ViewChild, ViewEncapsulation } from "@angular/core";
import { CreateContactUsComponent } from "./create-contact-us/create-contact-us.component";
import { Component, OnInit } from "@angular/core";
import { ContactUsService } from "../services/contact-us.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  ColumnMode,
  DatatableComponent,
  SelectionType,
} from "@swimlane/ngx-datatable";
import { Subject } from "rxjs";
import Swal from 'sweetalert2';
import { ViewContactUsComponent } from "./view-contact-us/view-contact-us.component";

@Component({
  selector: "app-contact-us",
  templateUrl: "./contact-us.component.html",
  styleUrls: ["./contact-us.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class ContactUsComponent implements OnInit {
  public basicSelectedOption: number[] = PER_PAGE_LIMIT;
  public usersRows: any;
  public ColumnMode = ColumnMode;
  public SelectionType = SelectionType;
  public pageInfo = { limit: 10, page: 1 };
  public contactUsList: any[] = [];
  public paginationConfig = {
    size: "",
    maxSize: 0,
  };
  public filterParams = {
    q: undefined,
    sortBy: undefined,
    page: undefined,
    limit: 5,
    status: undefined,
  };

  searchTerm$ = new Subject<any>();

  @HostListener("window:resize", ["$event"])
  onResize(event: any) {
    if (event.target.innerWidth <= 425) {
      this.paginationConfig.maxSize = 3;
      this.paginationConfig.size = "sm";
    } else {
      this.paginationConfig.maxSize = 0;
      this.paginationConfig.size = "";
    }
  }
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private contactUsService: ContactUsService,
    private modalService: NgbModal,
    private _sanitizer: DomSanitizer
  ) {
    if (window.innerWidth <= 425) {
      this.paginationConfig.maxSize = 3;
      this.paginationConfig.size = "sm";
    } else {
      this.paginationConfig.maxSize = 0;
      this.paginationConfig.size = "";
    }
  }

  ngOnInit(): void {
    // server-side search
    this.searchTerm$
      .pipe(
        map((event) => event.target.value),
        debounceTime(500)
      )
      .subscribe(() => {
        this.getContactUsList(this.filterParams);
      });
    this.getContactUsList(this.filterParams);
  }

  getContactUsList(params: any = {}): void {
    this.contactUsService.getContactList(params).subscribe((res: any) => {
      if (res.status) {
        this.contactUsList = res?.data?.data;
        !!this.contactUsList ? this.contactUsList : [];
        this.pageInfo.page = res?.data.page;
        this.pageInfo.limit = res?.data.limit;
        this.pageInfo["total"] = res?.data.total;
        this.filterParams.limit = res?.data.limit;
      }
    });
  }

  create() {
    const modelRef = this.modalService.open(CreateContactUsComponent, {
      centered: true,
    });

    let data = { test: "test" }; // set value
    modelRef.componentInstance.data = data; // model open pass data
    modelRef.result.then(
      (data: any) => {
        this.getContactUsList(this.filterParams);
      },
      () => {}
    );
  }

  delete(id: any){
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      customClass: {
        confirmButton: "btn btn-primary",
        cancelButton: "btn btn-danger ml-1",
      },
    }).then((result: any) => {
      if (result.isConfirmed) {
        this.contactUsService.deleteContactUS(id).subscribe(
          (res: any) => {
            this.filterParams.page = 1;
            this.getContactUsList(this.filterParams);
          },
          (error) => {console.log('error :>> ', error);}
        )
      }})
  }

  filterUpdate(event) {
    const val = event.target.value.toLowerCase();
    this.filterParams.q = val == "" ? undefined : val.trimStart();
    this.filterParams.page = 1;
  }

  pageChange(pagenumber: any) {
    this.filterParams.page = pagenumber;
    this.getContactUsList(this.filterParams);
  }

  limitChange(event: any) {
    const limit = event.target.value;
    this.filterParams.page = 1;
    this.filterParams.limit = limit;
    this.getContactUsList(this.filterParams);
  }

  byPassSecurity(data: any) {
    return this._sanitizer.bypassSecurityTrustUrl(data);
  }


  view(contactData: any){
    const modelRef = this.modalService.open(ViewContactUsComponent, {
      centered: true,
    });

    let data =  contactData ; // set value
    modelRef.componentInstance.data = data; // model open pass data
    modelRef.result.then(
      (data: any) => {
        // this.getContactUsList(this.filterParams);
      },
      () => {}
    );
  }
}
