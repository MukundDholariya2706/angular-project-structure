import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { AppModule } from "./../../../app.module";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { BrowserDynamicTestingModule } from "@angular/platform-browser-dynamic/testing";

import { CreateContactUsComponent } from "./create-contact-us.component";

describe("CreateContactUsComponent", () => {
  let component: CreateContactUsComponent;
  let fixture: ComponentFixture<CreateContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateContactUsComponent],
      imports: [BrowserDynamicTestingModule, AppModule],
      providers: [NgbModal, NgbActiveModal],
    }).compileComponents();

    fixture = TestBed.createComponent(CreateContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
