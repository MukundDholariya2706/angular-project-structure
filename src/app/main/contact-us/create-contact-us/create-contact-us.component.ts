import { HttpClient } from '@angular/common/http';
import { ToastrService } from "ngx-toastr";
import { ContactUsService } from "./../../services/contact-us.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-create-contact-us",
  templateUrl: "./create-contact-us.component.html",
  styleUrls: ["./create-contact-us.component.scss"],
})
export class CreateContactUsComponent implements OnInit {
  @Input() private data: any; // Receive Data - Pass by Model open
  @ViewChild("fileInput") fileInputad!: ElementRef;

  submitted: boolean = false;
  fileType: string[] = ["image/png", "image/jpeg"];
  imageSrc: string;
  uploadFileData: any;

  public createContactUsForm: FormGroup;

  get f() {
    return this.createContactUsForm.controls;
  }

  constructor(
    private model: NgbActiveModal,
    private _formBuilder: FormBuilder,
    private _contactUsService: ContactUsService,
    private _toasterService: ToastrService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.createContactUsForm = this._formBuilder.group({
      username: ["", [Validators.required]],
      query: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      contactUsImage: ["", [Validators.required]],
    });
  }

  modelDismiss(): void {
    this.model.dismiss();
  }

  onImagChangeFromFile(event: any): void {
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];
      this.uploadFileData = file;
      if (this.fileType.indexOf(file.type) >= 0) {
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
          reader.onload = (e: any) => {
            this.imageSrc = e.target.result;
            this.createContactUsForm.patchValue({
              contactUsImage: file,
            });
          };
      } else {
        this.imageSrc = '';
        this.uploadFileData = undefined;
        this.createContactUsForm.controls.contactUsImage.reset();
        const message = file.type + " file type not support";
        this._toasterService.error("", message, {
          toastClass: "toast ngx-toastr",
          closeButton: true,
        });
      }
    }
  }

  submitData(): void {
    this.submitted = true;
    if (this.createContactUsForm.invalid) return;

    const formData = new FormData();
    for(let key in this.createContactUsForm.value){
     formData.append(key, this.createContactUsForm.value[key]);
    }

    this._contactUsService.addContactList(formData).subscribe(
        (data: any) => {
          if(data.status){
            this.model.close();
          }
        },
        (error: any) => {
          console.log("error", error);
        }
      );

  }
}
