import { CoreCommonModule } from './../../../@core/common.module';
import { RouterModule } from '@angular/router';
import { Route } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FaqComponent } from './components/faq/faq.component';
import { CategoryComponent } from './components/category/category.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { AddQuestionAnswerComponent } from './components/add-question-answer/add-question-answer.component';

const route: Route[] = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: FaqComponent,
  },
  {
    path: 'category',
    component: CategoryComponent
  }
]

@NgModule({
  declarations: [
    FaqComponent,
    CategoryComponent,
    AddQuestionAnswerComponent
  ],
  imports: [
    CommonModule,
    CoreCommonModule,
    NgbModule,
    NgxDatatableModule,
    RouterModule.forChild(route),
    NgSelectModule
  ]
})
export class FaqModule { }
