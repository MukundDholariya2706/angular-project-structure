import { AddQuestionAnswerComponent } from './../add-question-answer/add-question-answer.component';
import { FaqService } from './../../service/faq.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import Swal from "sweetalert2";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FaqComponent implements OnInit {
  public faqList = [];
  public categoryList: any[] = [];
  public selectedCategory: string;

  constructor(private _faqService: FaqService, private modalService: NgbModal) { }

  ngOnInit(): void {
    // this.getQuestionList();
    this.getCategoryList();
  }

  getCategoryList(){
    const query = {
      isActive: true
    }
    this._faqService.getCategoryList(query).subscribe(
      (res: any) => {
        if(res.status === 1 && res.data.length > 0){
          this.categoryList = res.data;
        }
      }
    )
  }

  getQuestionList(event: any){
    if(!event || !this.selectedCategory ) return;
    this._faqService.getFaqByCategory(this.selectedCategory).subscribe(
      (res: any) => {
        if(res.status == 1){
          this.faqList = res.data;
        }
      }
    )
  }

  deleteQuestion(questionId: string){
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      customClass: {
        confirmButton: "btn btn-primary",
        cancelButton: "btn btn-danger ml-1",
      },
    }).then((result: any) => {
      if (result.isConfirmed) {
        this._faqService.deleteQuestion(questionId).subscribe(
          (res: any) => {
            console.log('res :>> ', res);
            if(res.status == 1){
              this.getQuestionList(1);
            }
          }
        )
      }
    })
  }


  addEditQuestion(id: string = undefined){
    const modelRef = this.modalService.open(AddQuestionAnswerComponent, {
      centered: true
    })

    if(!!id){
      modelRef.componentInstance.questionId = id;
    }

    modelRef.result.then(
      () => {
        this.getQuestionList(1);
      },
      () => {}
    )
  }

}
