import { FaqService } from "./../../service/faq.service";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import Swal from "sweetalert2";

@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class CategoryComponent implements OnInit {
  public categoryList = [];
  public categoryName: string;
  public editing = {};
  rows = [];

  constructor(private _faqService: FaqService) {}

  ngOnInit(): void {
    this.getCatagoryList();
  }

  getCatagoryList() {
    this._faqService.getCategoryList().subscribe((res: any) => {
      if (res.status == 1) {
        this.categoryList = res.data;
      }
    });
  }

  createCategory() {
    if (!this.categoryName) return;

    this.categoryName = capitalizeFirstLetter(this.categoryName)

    this._faqService
      .createCategory({ name: this.categoryName })
      .subscribe((res: any) => {
        if (res.status == 1) {
          this.categoryList = [...this.categoryList, res.data];
          this.categoryName = undefined;
        }
      });
  }

  updateValue(event: any, cell: any, rowIndex: any) {
    this.editing[rowIndex + "-" + cell] = false;
    if (
      this.categoryList[rowIndex][cell] == event.target.value ||
      event.target.value == ""
    ) {
      this.categoryList[rowIndex][cell] = !!event.target.value
        ? event.target.value
        : this.categoryList[rowIndex][cell];
      this.categoryList = [...this.categoryList];
    } else {
      const formData = {
        name: capitalizeFirstLetter(event.target.value),
      };
      this._faqService
        .editCategory(formData, this.categoryList[rowIndex]._id)
        .subscribe((res: any) => {
          if (res.status == 1) {
            this.categoryList[rowIndex] = res.data;
            this.categoryList = [...this.categoryList];
          } else {
            this.categoryList[rowIndex][cell] =
              this.categoryList[rowIndex][cell];
          }
        });
    }
  }

  statusChange(event: Event, id: any) {
    let active = event.target["checked"] ? "1" : "2";
    const formData = {
      status: active,
    };
    this._faqService.editCategoryStatus(formData, id).subscribe((res: any) => {
      if (res.status == 1) {
        const index = this.categoryList.findIndex((a: any) => a._id == id);
        this.getCatagoryList();
      } else {
        this.getCatagoryList();
      }
    });
  }

  deleteCategory(id: any) {
    Swal.fire({
      title: "Are you sure?",
      text: "You want to delete this?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      customClass: {
        confirmButton: "btn btn-primary",
        cancelButton: "btn btn-danger ml-1",
      },
    }).then((result: any) => {
      if (result.isConfirmed) {
        this._faqService.deleteCategory(id).subscribe((res: any) => {
          if (res.status) {
            this.getCatagoryList();
          }
        });
      }
    });
  }
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}