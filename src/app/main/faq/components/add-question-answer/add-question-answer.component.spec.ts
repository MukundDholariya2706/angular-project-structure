import { AppModule } from './../../../../app.module';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddQuestionAnswerComponent } from './add-question-answer.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

describe('AddQuestionAnswerComponent', () => {
  let component: AddQuestionAnswerComponent;
  let fixture: ComponentFixture<AddQuestionAnswerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddQuestionAnswerComponent ],
      imports: [AppModule],
      providers: [NgbActiveModal]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddQuestionAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
