import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { FaqService } from './../../service/faq.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'app-add-question-answer',
  templateUrl: './add-question-answer.component.html',
  styleUrls: ['./add-question-answer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddQuestionAnswerComponent implements OnInit {
  @Input() public questionId: any;

  public createForm: FormGroup;
  public submitted: boolean = false;
  public categoryList = [];

  get f(){
    return this.createForm.controls;
  }

  constructor(
    private _formBuilder: FormBuilder,
    private model: NgbActiveModal,
    private _faqService: FaqService
  ) { }

  ngOnInit(): void {
    if(!!this.questionId) { 
      this.getQuestionDate();
    }
    this.getCategoryList();
    this.initForm();
  }

  getQuestionDate(){
    this._faqService.getQuestionById(this.questionId).subscribe(
      (res: any) => {
        if(res.status === 1){
          this.patchValue(res.data[0]);
        }
      }
    )
  }

  patchValue(data: any = {}){
    this.createForm.patchValue({
      categoryId: data.categoryData._id,
      questionText: data.questionText,
      answer: data.answer
    })
  }

  getCategoryList(){
    const query = {
      isActive: true
    }
    this._faqService.getCategoryList(query).subscribe(
      (res: any) => {
        if(res.status === 1 && res.data.length > 0){
          this.categoryList = res.data;
        }
      }
    )
  }

  initForm(): void {
    this.createForm = this._formBuilder.group({
      categoryId: [null, [Validators.required]],
      questionText: ["", [Validators.required]],
      answer: ["", [Validators.required]]
    })
  }

  modelDismiss(){
    this.model.dismiss();
  }

  submitData(){
    this.submitted = true;
    if(this.createForm.invalid) return;
    let API: any;
    if(!!this.questionId){
      API = this._faqService.editQuestion(this.createForm.value, this.questionId);
    } else {
      API = this._faqService.addQuestion(this.createForm.value);
    }

    API.subscribe(
      (res: any) => {
        if(res.status == 1){
          this.model.close();
        }
      }
    )
  }
}
