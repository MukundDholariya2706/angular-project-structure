import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthGuard } from 'app/auth/helpers';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { AccountSettingsComponent } from 'app/main/pages/account-settings/account-settings.component';
import { AccountSettingsService } from 'app/main/pages/account-settings/account-settings.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { GeneralComponent } from './general/general.component';
import { NotificationComponent } from './notification/notification.component';
import { BlockUIModule } from 'ng-block-ui';
import { SharedModule } from 'app/shared/shared.module';

const routes: Routes = [
  {
    path: 'account-settings',
    component: AccountSettingsComponent,
    canActivate: [AuthGuard],
    data: { animation: 'fadeIn' },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'general'
      },
      {
        path: 'general',
        component: GeneralComponent
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'notification',
        component: NotificationComponent
      }
    ]
  }
];

@NgModule({
  declarations: [AccountSettingsComponent, ChangePasswordComponent, GeneralComponent, NotificationComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbModule,
    CoreCommonModule,
    ContentHeaderModule,
    Ng2FlatpickrModule,
    BlockUIModule,
    SharedModule
  ],
  providers: [AccountSettingsService]
})
export class AccountSettingsModule {}
