import { ImgCropperComponent } from "./../../../../shared/components/img-cropper/img-cropper.component";
import { CoreService } from "./../../../../core/services/core.service";
import { Subject } from "rxjs";
import { AccountSettingsService } from "app/main/pages/account-settings/account-settings.service";
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation,
} from "@angular/core";
import { User } from "app/auth/models";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Address } from "ngx-google-places-autocomplete/objects/address";
var PhoneNumber = require( 'awesome-phonenumber' );

@Component({
  selector: "app-general",
  templateUrl: "./general.component.html",
  styleUrls: ["./general.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class GeneralComponent implements OnInit, OnDestroy {
  @Output() currentTab = new EventEmitter<{ name: string; link: string }>();
  @BlockUI() blockUI: NgBlockUI;

  public avatarImage: string;
  public patchProfileImage: string = "";
  public userData: User;
  public userDetailsForm: FormGroup;
  public submitted = false;
  public countryList: any[] = [];
  public phoneNumberMask: string;
  public phonePlaceholder: string;
  // private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private modalService: NgbModal,
    private _coreService: CoreService,
    private _accountSettingsService: AccountSettingsService,
    private _formBuilder: FormBuilder
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.currentTab.emit({ name: "General", link: "general" });
    this.userData = JSON.parse(localStorage.getItem("currentUser"));
    this.avatarImage = this.userData?.profile_image;
    this.initForm();
    this._accountSettingsService.getCountryList().subscribe((response) => {
      this.countryList = response["data"];
    });
    this.getUserDetails();
    this.countryChange();
  }

  get f() {
    return this.userDetailsForm.controls;
  }

  get g() {
    return (this.f.contact_details as FormGroup).controls;
  }

  initForm() {
    this.userDetailsForm = this._formBuilder.group({
      first_name: ["", [Validators.required]],
      last_name: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      company: ["", [Validators.required]],
      address: ["", [Validators.required]],
      contact_details: this._formBuilder.group({
        dial_code: [null, [Validators.required]],
        country_code: ["", [Validators.required]],
        mobile_number: ["", [Validators.required]],
      }),
      profile_image: [""],
    });
  }

  /**
   * get User Profile
   */
  getUserDetails(): void {
    this._accountSettingsService.getUserProfile().subscribe((res: any) => {
      if (res.status == 1) {
        this.userData = res.data;
        this.patchValue(res.data);
      }
    });
  }

  patchValue(data) {
    const file = new File([data?.profile_image], "");

    this.userDetailsForm.patchValue({
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      company: data.company,
      address: data.address,
      profile_image: file,
      contact_details: {
        dial_code: data?.contact_details?.dial_code,
        country_code: data?.contact_details?.country_code,
        mobile_number: data?.contact_details?.mobile_number,
      },
    });
    this.patchProfileImage = data?.profile_image;
    this.avatarImage = data?.profile_image;
  }

  /**
   * Upload Image
   *
   * @param event
   */
  // not use
  uploadImage(event: any) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      let reader = new FileReader();

      reader.onload = (event: any) => {
        this.avatarImage = event.target.result;
        this.userDetailsForm.patchValue({
          profile_image: file,
        });
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  resetImage() {
    this.f?.profile_image.patchValue(this.patchProfileImage);
  }

  onSubmit() {
    this.submitted = true;
    if (this.userDetailsForm.invalid) return;

    const formData = new FormData();
    for (let key in this.userDetailsForm.value) {
      if (key != "contact_details") {
        formData.append(key, this.userDetailsForm.value[key]);
      } else {
        formData.append(
          "contact_details.dial_code",
          this.userDetailsForm.value[key].dial_code
        );
        formData.append(
          "contact_details.country_code",
          this.userDetailsForm.value[key].country_code
        );
        formData.append(
          "contact_details.mobile_number",
          this.userDetailsForm.value[key].mobile_number
        );
      }
    }

    this._accountSettingsService.updateUserProfile(formData).subscribe(
      (data: any) => {
        if (data.status) {
          // data.data = {...data.data, role: data.data.user_type == 1 ? 'Admin' : 'Client'};
          // this._jwtService.setLocalStore('currentUser',data.data);
          this._coreService.profileChanged.next({
            profile_image: data.data?.profile_image,
            first_name: data.data.first_name,
            last_name: data.data.last_name,
          });
          this._coreService.currentUserData.next(data.data);
          this.getUserDetails();
        }
      },
      (error: any) => {
        this.blockUI.stop();
      }
    );
  }

  openImageCropper() {
    const modelRef = this.modalService.open(ImgCropperComponent, {
      centered: true,
    });

    modelRef.componentInstance.coverImageWeb = this.avatarImage;
    modelRef.componentInstance.aspectHeight = 4;
    modelRef.componentInstance.aspectWidth = 4;
    modelRef.componentInstance.roundCropper = false;


    modelRef.result.then(
      (file: File) => {
        if (!file) return;
        let reader = new FileReader();
        reader.onload = (event: any) => {
          this.avatarImage = event.target.result;
          this.userDetailsForm.patchValue({
            profile_image: file,
          });
        };
        reader.readAsDataURL(file);
      },
      () => {}
    );
  }

  handleAddressChange(address: Address){
    this.userDetailsForm.patchValue({
      address: address.formatted_address
    })
  }

  // phone number validation

  countryChange(){
    this.g.country_code.valueChanges.subscribe(
      (value) => {
        if(value) {
          this.setMaskAndExample(value.slice(0,2));
          this.g.dial_code.patchValue(this.getCountryCodeFormRegionCode(value.slice(0,2)), {
            emitEvent: false,});
        }
      }
    )
  }

  getCountryCodeFormRegionCode(regionCode: string): string {
    return PhoneNumber.getCountryCodeForRegionCode(regionCode);
  }

  setMaskAndExample(regionCode: string){
    this.phonePlaceholder = this.getPlaceholder(regionCode);
    this.phoneNumberMask = this.getMask(regionCode);
  }

  getMask(regionCode: string, format = 'national'): string {
    return this.getPlaceholder(regionCode, format).replace(/\d/g, '0');
  }

  getPlaceholder(regionCode: string, format = 'national'): string {
    console.log('regionCode :>> ', regionCode);
    let example = PhoneNumber.getExample(regionCode);
    return example?.number?.national;
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
