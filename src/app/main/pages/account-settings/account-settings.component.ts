import { ContentHeader } from "./../../../layout/components/content-header/content-header.component";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "app-account-settings",
  templateUrl: "./account-settings.component.html",
  styleUrls: ["./account-settings.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class AccountSettingsComponent implements OnInit {
  // public
  public contentHeader: ContentHeader;
  public activeTab: any;

  constructor(private router: Router) {
    this.activeTab = this.router.url.split("/").pop();
  }

  ngOnInit() {
    this.contentHeader = {
      headerTitle: "Account Settings",
      actionButton: true,
      breadcrumb: {
        links: [
          {
            name: "Account Settings",
            isLink: false,
          },
        ],
      },
    };
  }

  activeBreadcrumb(event: any) {
    if (this.contentHeader.breadcrumb.links.length > 0 && this.contentHeader.breadcrumb.links.length <= 1) {
      this.contentHeader.breadcrumb.links.push({
        name: event.name,
        isLink: false,
        link: event.link,
      });
    } else {
      this.contentHeader.breadcrumb.links[1].name = event.name;
      this.contentHeader.breadcrumb.links[1].link = event.link;
    }
  }
}
