import { EventEmitter, OnDestroy, Output } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { AccountSettingsService } from 'app/main/pages/account-settings/account-settings.service';
import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {
  @Output() currentTab = new EventEmitter<{name: string, link: string}>();

  // private
  private _unsubscribeAll: Subject<any>;

  constructor(private _accountSettingsService: AccountSettingsService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.currentTab.emit({name: 'Notification', link: 'notification'})
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
