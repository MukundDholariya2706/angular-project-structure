import { ResponseModel } from './../../../core/models/response.model';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { API_LINK_URL } from 'app/API-URL/contants';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from 'app/core/api/base-api.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Injectable()
export class AccountSettingsService {
  @BlockUI() blockUI: NgBlockUI;

  constructor(private _http: HttpClient, private _baseApiService: BaseApiService, private _toasterService: ToastrService) {}

  changePassword(postData: any){
    return this._baseApiService.makeRequest('POST','users/change-password',postData);
  }

  // not use API 
  // updateUserProfile(data: any){
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'multipart/form-data; application/x-www-form-urlencoded; charset=UTF-8'
  //   });

  //   return this._baseApiService.makeRequest('PUT','users/', data,"json", headers);
  // }

  updateUserProfile(data: any){
    this.blockUI.start('Loading...');
    return this._http.put(API_LINK_URL+'users/',data).pipe(
      map((res: ResponseModel) => {
        this.blockUI.stop();
        if(res?.status == 1){
          this._toasterService.success('',res.message, { toastClass: 'toast ngx-toastr', closeButton: true })
        }
        if(res?.status == 0 ){
          this._toasterService.error('',res.message, { toastClass: 'toast ngx-toastr', closeButton: true })
        }
        return res;
      })
    );
  }

  getCountryList(){
    return this._baseApiService.makeRequest('GET','country/countryList');
  }

  getUserProfile(){
    return this._baseApiService.makeRequest('GET','users/profile');
  }
  
}
