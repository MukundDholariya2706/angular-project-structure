import { AccountSettingsService } from 'app/main/pages/account-settings/account-settings.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { MustMatch, NotMatch } from "../../../../core/helper/password.validator";
@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.scss"],
})
export class ChangePasswordComponent implements OnInit {

  @Output() currentTab = new EventEmitter<{ name: string; link: string; }>();

  //public
  public passwordTextTypeOld = false;
  public passwordTextTypeNew = false;
  public passwordTextTypeRetype = false;
  public submitted = false;
  public loading: boolean = false;
  public changePasswordForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private _accountSettingsService: AccountSettingsService) {}

  // convenience getter for easy access to form fields
  get f() {
    return this.changePasswordForm.controls;
  }

  ngOnInit(): void {
    this.currentTab.emit({name: 'Change Password', link: 'change-password'});
    this.intiForm();
  }

  /**
   * Toggle Password Text Type Old
   */
  togglePasswordTextTypeOld() {
    this.passwordTextTypeOld = !this.passwordTextTypeOld;
  }

  /**
   * Toggle Password Text Type New
   */
  togglePasswordTextTypeNew() {
    this.passwordTextTypeNew = !this.passwordTextTypeNew;
  }

  /**
   * Toggle Password Text Type Retype
   */
  togglePasswordTextTypeRetype() {
    this.passwordTextTypeRetype = !this.passwordTextTypeRetype;
  }

  intiForm() {
    this.changePasswordForm = this.formBuilder.group({
      old_password: ["", [Validators.required]],
      new_password: ["", [Validators.required]],
      confirm_password: ["", [Validators.required]],
    },
    {
      validator: [MustMatch('new_password', 'confirm_password'), NotMatch('old_password','new_password')]
      
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if(this.changePasswordForm.invalid) return;

    this.loading = true;
    this._accountSettingsService.changePassword(this.changePasswordForm.value).subscribe((res: any) => {
      if(!!res.status){
        this.loading = false;
      }
    },
    (error) => {
      this.loading = false;
    });
  }
}
