import { ResponseModel } from "app/core/models/response.model";
import { ActivatedRoute } from "@angular/router";
import { AccountSettingsService } from "app/main/pages/account-settings/account-settings.service";
import { ContentHeader } from "app/layout/components/content-header/content-header.component";
import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { map } from "rxjs/operators";
import { CoreService } from "app/core/services/core.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ImgCropperComponent } from "app/shared/components/img-cropper/img-cropper.component";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit, OnDestroy {
  // public
  public contentHeader: ContentHeader;
  public toggleMenu = true;
  public Monthly = false;
  public toggleNavbarRef = false;
  public loadMoreRef = false;
  public userData: any;
  public showLoader = false;
  public coverImage = "assets/images/profile/user-uploads/timeline.jpg";

  constructor(
    private _accountSettingsService: AccountSettingsService,
    private route: ActivatedRoute,
    private _coreService: CoreService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.userData = this.route.snapshot.data.currentUser;
    this.userData = {...this.userData, joined: new Date(this.userData.created_at * 1000)};

    // content header
    this.contentHeader = {
      headerTitle: "Profile",
      actionButton: true,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Pages",
            isLink: true,
            link: "/",
          },
          {
            name: "Profile",
            isLink: false,
          },
        ],
      },
    };

    // this.getProfileDetails();
  }

  getProfileDetails(): void {
    this._accountSettingsService
      .getUserProfile()
      .pipe(
        map((response: ResponseModel) => {
          response.data.joined = new Date(response.data.created_at * 1000);
          return response;
        })
      )
      .subscribe((response: ResponseModel) => {
        if (response.status === 1) {
          console.log('response :>> ', response);
          this.userData = response["data"];
        }
      });
  }

  // not use
  uploadCoverImage(event: any) {
    let file: any;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      const reader = new FileReader();

      reader.onload = (event: any) => {
        this.coverImage = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }

    const formData = new FormData();
    formData.append("cover_image", file);
    this._accountSettingsService
      .updateUserProfile(formData)
      .subscribe((res: any) => {
        if (res.status == 1) {
          this.userData = res.data;
          this._coreService.currentUserData.next(res.data);
        }
      });
  }

  openImageCropper() {
    const modelRef = this.modalService.open(ImgCropperComponent, {
      centered: true,
    });

    modelRef.componentInstance.coverImageWeb = this.userData.cover_image ? this.userData.cover_image : this.coverImage;
    modelRef.componentInstance.aspectHeight = 5;
    modelRef.componentInstance.aspectWidth = 16;
    modelRef.componentInstance.roundCropper = false;

    modelRef.result.then(
      (file: File) => {
        const reader = new FileReader();
        reader.onload = (event: any) => {
          this.coverImage = event.target.result;
        };
        reader.readAsDataURL(file);

        const formData = new FormData();
        formData.append("cover_image", file);
        this._accountSettingsService
          .updateUserProfile(formData)
          .subscribe((res: any) => {
            if (res.status == 1) {
              this.userData = res.data;
              this._coreService.currentUserData.next(res.data);
            }
          });
      },
      () => {}
    );
  }

  ngOnDestroy(): void {}
}
