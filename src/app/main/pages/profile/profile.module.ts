import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AuthGuard } from "app/auth/helpers";
import { CoreCommonModule } from "@core/common.module";
import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";

import { ProfileComponent } from "app/main/pages/profile/profile.component";
import { LayoutModule } from "../../../layout/layout.module";
import { BlockUIModule } from "ng-block-ui";
import { ProfileResolver } from "./resolver/profile.resolver";
import { SharedModule } from "app/shared/shared.module";

const routes: Routes = [
  {
    path: "profile",
    component: ProfileComponent,
    canActivate: [AuthGuard],
    resolve: {
      currentUser: ProfileResolver,
    },
  },
];

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbModule,
    CoreCommonModule,
    ContentHeaderModule,
    LayoutModule,
    BlockUIModule,
    SharedModule,
  ],
})
export class ProfileModule {}
