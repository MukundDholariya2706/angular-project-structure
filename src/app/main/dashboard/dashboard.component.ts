import { SliderManagementService } from './../services/slider-management.service';
import { ContentHeader } from "app/layout/components/content-header/content-header.component";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  // public
  public contentHeader: ContentHeader;
  public sliderList: any[] = [];
  public swiperAutoplay: SwiperConfigInterface = {
    spaceBetween: 30,
    loop: true,
    centeredSlides: true,
    autoplay: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  };

  constructor(private _sliderManagementService: SliderManagementService) {}

  ngOnInit() {
    this.contentHeader = {
      headerTitle: "Dashboard",
      actionButton: false
    };

    this.getDashboardData();
  }

  getDashboardData(){
    this._sliderManagementService.getAllSlider({isActive: true}).subscribe((res: any) => {
      if(res.status){
        this.sliderList = res.data;
      }
    })
  }
}
