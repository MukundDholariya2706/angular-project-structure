import { CoreMenu } from "@core/types";

//? DOC: http://localhost:7777/demo/vuexy-angular-admin-dashboard-template/documentation/guide/development/navigation-menus.html#interface

export const menu: CoreMenu[] = [
  // Dashboard
  {
    id: "dashboard",
    title: "Dashboard",
    translate: "MENU.DASHBOARD.COLLAPSIBLE",
    type: "item",
    // role: ['Admin'], //? To hide collapsible based on user role
    icon: "home",
    url: "dashboard",
  },

  {
    id: "users",
    title: "Users",
    translate: "MENU.USERS.COLLAPSIBLE",
    type: "collapsible",
    icon: "user",
    role: ['Admin'],
    children: [
      {
        id: "all",
        title: "All",
        translate: "MENU.USERS.ALL",
        type: "item",
        icon: "circle",
        url: "users/all",
      },
      {
        id: "admin",
        title: "Admin",
        translate: "MENU.USERS.ADMIN",
        type: "item",
        icon: 'circle',
        url: "users/admin",
      },
    ],
  },
  // Apps & Pages
  {
    id: "apps",
    type: "section",
    title: "Management",
    translate: "MENU.APPS.SECTION",
    icon: "package",
    children: [
      {
        id: "slider-management",
        title: "Slider Management",
        translate: "MENU.PAGES.SLIDER",
        type: "item",
        // icon: 'circle',
        url: "slider-management",
      },
      {
        id: "contact-us",
        title: "Contact Us",
        translate: "MENU.PAGES.CONTACTUS",
        type: "item",
        // icon: 'circle',
        url: "contact-us",
        // collapsed: true
      },
      {
        id: "about-us",
        title: "About Us",
        translate: "MENU.PAGES.ABOUTUS",
        type: "item",
        // icon: 'circle',
        url: "about-us",
      },
    ],
  },
  {
    id: "faq",
    title: "FAQ",
    translate: "MENU.PAGES.ABOUTUS",
    type: "collapsible",
    icon: 'help-circle',
    children: [
      {
        id: "faq-list",
        title: "List",
        type: "item",
        icon: "circle",
        url: "faq/list",
      },
      {
        id: "all",
        title: "Category",
        type: "item",
        icon: "circle",
        url: "faq/category",
      }
    ]
  },
];
