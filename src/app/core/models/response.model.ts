export interface ResponseModel {
    message: string;
    status: number;
    data: any;
}
