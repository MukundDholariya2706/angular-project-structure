import { JwtService } from "./jwt.service";
import { Subject } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class CoreService {
  profileChanged = new Subject();
  currentUserData = new Subject();

  constructor(private _jwtService: JwtService) {
    this.currentUserData.subscribe((res: any) => {
      console.log('res subject :>> ', res);
      res = {
        ...res,
        role: res.user_type == 1 ? "Admin" : "Client",
      };
      this._jwtService.setLocalStore("currentUser", res);
    });
  }
}
