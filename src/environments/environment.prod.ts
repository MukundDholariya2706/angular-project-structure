import { production } from "../app/API-URL/API-Url-Config";

export const environment = {
  production: true,
  hmr: false,
  apiUrl: production.API_LiNK
};
