import { NgModule } from '@angular/core';

import { FeatherIconDirective } from '@core/directives/core-feather-icons/core-feather-icons';
import { RippleEffectDirective } from '@core/directives/core-ripple-effect/core-ripple-effect.directive';
import { PermissionDirective } from '@core/directives/permission/permission.directive';

@NgModule({
  declarations: [RippleEffectDirective, FeatherIconDirective, PermissionDirective],
  exports: [RippleEffectDirective, FeatherIconDirective, PermissionDirective]
})
export class CoreDirectivesModule {}
